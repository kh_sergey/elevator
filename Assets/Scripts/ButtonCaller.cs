using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ButtonCaller : MonoBehaviour
{
    [SerializeField] private string _callDirection;
    [SerializeField] private MeshRenderer _sameDirectionLights;
    private Material _material;
    private Floor _floor;

    private void Awake()
    {
        _material = GetComponent<MeshRenderer>().material;
        _floor = GetComponentInParent<Floor>();
    }

    private void OnMouseDown()
    {
        MakeHitColor();
        MakeLightsColor(Color.yellow);
        _floor.CallElevator(_callDirection);
    }

    public void MakeHitColor()
    {
        Sequence colorSequence = DOTween.Sequence();
        colorSequence.Append(_material.DOColor(Color.yellow, 0.2f));
        colorSequence.Append(_material.DOColor(Color.black, 0.2f));
    }

    public void MakeLightsColor(Color color)
    {
        _sameDirectionLights.material.DOColor(color, 0.2f);
    }
}
