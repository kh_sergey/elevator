using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ButtonSelector : MonoBehaviour
{
    [SerializeField] private int numberSelector;
    [SerializeField] private MeshRenderer lights;
    private ButtonsSet _buttonsSet;
    private Camera camera;

    private void Awake()
    {
        _buttonsSet = GetComponentInParent<ButtonsSet>();
        camera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    private void OnMouseDown()
    {
        _buttonsSet.ToggleButton(numberSelector);
    }

    public void ToggleLights(bool state)
    {
        if (state)
        {
            MakeLightsColor(Color.yellow);
        } else
        {
            MakeLightsColor(Color.white);
        }
    }

    public void MakeLightsColor(Color color)
    {
        lights.material.DOColor(color, 0.2f);
    }
}
