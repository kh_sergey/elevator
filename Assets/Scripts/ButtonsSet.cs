using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ButtonsSet : MonoBehaviour
{
    [SerializeField] private List<ButtonSelector> _buttonsWithLights;
    private QueueFloorsList _queueFloorsList;
    private Elevator _elevator;

    private void Awake()
    {
        _queueFloorsList = GetComponentInParent<QueueFloorsList>();
        _elevator = GetComponentInParent<Elevator>();
    }

    private void FixedUpdate()
    {
        _buttonsWithLights[0].ToggleLights(_queueFloorsList.Contains(1));
        _buttonsWithLights[1].ToggleLights(_queueFloorsList.Contains(2));
        _buttonsWithLights[2].ToggleLights(_queueFloorsList.Contains(3));
        _buttonsWithLights[3].ToggleLights(_queueFloorsList.Contains(4));
    }

    public void ToggleButton(int floor)
    {
        // ��� ������� ������� � �� ��������
        if (!_elevator.OpenDoorProcess)
        {
            _elevator.AddFloorToQueue(floor);
        }
    }
}
