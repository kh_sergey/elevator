using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    public QueueFloorsList _queueFloorsList;
    public bool Busy { get; private set; } = false;
    public bool Moving { get; private set; } = false;
    public bool ReadyForMoving { get; private set; } = true;
    public bool CanStop;
    public bool OpenDoorProcess = false;
    public int _currentFloor = 1;
    public string _elevatorMoveDirection;
    private string _floorDirection;
    private Floor _floor;
    private FloorArc _floorArc;
    [SerializeField] private Engine _engine;
    [SerializeField] private ElevatorManager _elevatorManager;
    [SerializeField] private GameObject _door;

    private void Awake()
    {
        _queueFloorsList = GetComponent<QueueFloorsList>();
    }

    private void FixedUpdate()
    {
        if (_queueFloorsList.Count() > 0)
        {
            if (ReadyForMoving) 
            {
                MoveTo(_queueFloorsList.GetNextDestination(_elevatorMoveDirection, _currentFloor).Item1);
            }
        }
    }

    private void Update()
    {
        if (_queueFloorsList.Contains(_currentFloor))
        {
            if (CanStop)
            {
                StartCoroutine(OpenDoors());
            }
        } else if(_elevatorManager._calledFloors.Contains(_currentFloor) && Busy)
        {
            if (CanStop && (_floorDirection == _elevatorMoveDirection || _floorDirection == "both"))
            {
                StartCoroutine(OpenDoors());
            }
        }
        
    }

    public void ToggleCanStop(bool state)
    {
        CanStop = state;
    }

    public void AddFloorToQueue(int floor)
    {
        _queueFloorsList.Add(floor);
        ToggleElevatorBusy(true);
    }

    private void ToggleElevatorBusy(bool state)
    {
        Busy = state;
        if (Busy == true)
        {
            _elevatorManager._freeElevators.Remove(gameObject.GetComponent<Elevator>());
        } else if (Busy == false)
        {
            _elevatorManager._freeElevators.Add(gameObject.GetComponent<Elevator>());
            _elevatorMoveDirection = "none";
        }
    }

    private void ToggleElevatorMoving(bool state)
    {
        Moving = state;
        if (Moving == true)
        {
            _door.SetActive(true);
        } else
        {
            _door.SetActive(false);
        }
    }

    public void MoveTo(string destination)
    {
        ReadyForMoving = false;
        ToggleElevatorMoving(true);
        _elevatorMoveDirection = destination;
        _floorArc.ToggleShutters(true);
        if (_elevatorMoveDirection == "up")
        {
            _engine.Lifting();
        } else if (_elevatorMoveDirection == "down")
        {
            _engine.Descent();
        }
    }

    public void UpdateCurrentFloor(int currentFloor)
    {
        _currentFloor = currentFloor;
    }

    public IEnumerator OpenDoors()
    {
        OpenDoorProcess = true;
        if (_queueFloorsList.Contains(_floor.FloorNumber))
        {
            _floor.OffAllLightsWithDirection("both");
        }
        else
        {
            _floor.OffAllLightsWithDirection(_elevatorMoveDirection);
        }
        _engine.Stop();
        _floorArc.ToggleShutters(false);
        ToggleElevatorMoving(false);
        _queueFloorsList.Remove(_currentFloor);
        _elevatorManager._calledFloors.Remove(_currentFloor);

        yield return new WaitForSeconds(2f);

        if (_queueFloorsList.Count() == 0)
        {
            ToggleElevatorBusy(false);
        }
        OpenDoorProcess = false;
        ReadyForMoving = true;
    }

    private void OnTriggerEnter(Collider collider)
    {
        _floorDirection = collider.GetComponentInParent<Floor>().callDirection;
        _floor = collider.GetComponentInParent<Floor>();
        _floorArc = collider.GetComponentInParent<FloorArc>();
    }
}
