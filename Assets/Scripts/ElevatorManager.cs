using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorManager : MonoBehaviour
{
    //private FreeElevatorsList _freeElevators;
    //private CalledFloorsList _calledFloors;
    public List<Elevator> _freeElevators = new List<Elevator>();
    public List<int> _calledFloors = new List<int>();

    private void Awake()
    {
        //_freeElevators = GetComponent<FreeElevatorsList>();
        //_calledFloors = GetComponent<CalledFloorsList>();
    }

    
    public void AddCalledFloor(int floor)
    {
        _calledFloors.Add(floor);
    }

    
    private void FixedUpdate()
    {
        foreach(Elevator freeElevator in _freeElevators)
        {
            if (_calledFloors.Contains(freeElevator._currentFloor) && freeElevator.Busy == false)
            {
                _calledFloors.Remove(freeElevator._currentFloor);
            }
        }

        if (_calledFloors.Count > 0)
        {
            if (_freeElevators.Count > 0)
            {
                _freeElevators[0].AddFloorToQueue(_calledFloors[0]);
                _calledFloors.RemoveAt(0);
            }
        }
    }
}
