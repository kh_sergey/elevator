using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engine : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private GameObject _elevator;
    private Vector3 _direction;
    private bool _move = false;

    public void Lifting()
    {
        _direction = Vector3.up;
        _move = true;
    }

    public void Descent()
    {
        _direction = Vector3.down;
        _move = true;
    }

    public void Stop()
    {
        _move = false;
    }

    private void FixedUpdate()
    {
        if (_move)
        {
            _elevator.transform.position += _direction * _speed * Time.deltaTime;
        }
    }
}
