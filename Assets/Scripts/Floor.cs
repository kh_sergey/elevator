using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    [SerializeField] private int _floorNumber;
    [SerializeField] public ElevatorManager _elevatorManager;
    public int FloorNumber { get => _floorNumber; private set => _floorNumber = value; }
    public bool Called { get; private set; } = false;
    public string callDirection;
    private FloorArc[] _floorArcs;

    private void Awake()
    {
        _floorArcs = GetComponentsInChildren<FloorArc>();
    }

    public void CallElevator(string direction)
    {
        Called = true;
        if (callDirection == "none" || callDirection == direction)
        {
            callDirection = direction;
        } else if (callDirection == "both" || callDirection != direction)
        {
            callDirection = "both";
        }
        _elevatorManager.AddCalledFloor(FloorNumber);
    }
    
    public void OffAllLightsWithDirection(string direction)
    {
        foreach(FloorArc floorArc in _floorArcs)
        {
            floorArc.ShutdownLightsOfType(direction);
        }
    }
}
