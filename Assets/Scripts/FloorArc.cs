using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FloorArc : MonoBehaviour
{
    [SerializeField] private MeshRenderer _upLights;
    [SerializeField] private MeshRenderer _downLights;
    [SerializeField] private GameObject _shutters;

    public void ShutdownLightsOfType(string direction)
    {
        if (direction == "up")
        {
            MakeLightsBlack(_upLights);
        } else if (direction == "down")
        {
            MakeLightsBlack(_downLights);
        } else if (direction == "both")
        {
            MakeLightsBlack(_upLights);
            MakeLightsBlack(_downLights);
        }
    }
    public void ToggleShutters(bool state)
    {
        _shutters.SetActive(state);
    }

    private void MakeLightsBlack(MeshRenderer _lights)
    {
        _lights.material.DOColor(Color.black, 0.2f);
    }
}
