using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueueFloorsList : MonoBehaviour
{
    public List<int> queueFloors = new List<int>();
    private Elevator _elevator;

    private void Awake()
    {
        _elevator = GetComponent<Elevator>();
    }

    public int Count()
    {
        return queueFloors.Count;
    }

    public bool Contains(int floor)
    {
        return queueFloors.Contains(floor);
    }

    public (string, int) GetNextDestination(string direction, int currentFloor)
    {
        string newDirection = "none";
        int newFloor;
        if (direction == "none")
        {
            if (queueFloors[0] > currentFloor)
            {
                newDirection = "up";
            }
            else if (queueFloors[0] < currentFloor)
            {
                newDirection = "down";
            }
            newFloor = queueFloors[0];
        }
        else
        {
            newFloor = searchFirstMatchInDirection(direction, currentFloor);
            newDirection = direction;

            if (newFloor == 0)
            {
                if (direction == "up")
                {
                    newFloor = searchFirstMatchInDirection("down", currentFloor);
                    newDirection = "down";
                } else if (direction == "down")
                {
                    newFloor = searchFirstMatchInDirection("up", currentFloor);
                    newDirection = "up";
                }
            }
        }

        return (newDirection, newFloor);
    }

    private int searchFirstMatchInDirection(string direction, int currentFloor)
    {
        List<int> goodList = new List<int>();

        for (int i = 0; i < queueFloors.Count; i++)
        {
            if (direction == "up")
            {
                if (queueFloors[i] > currentFloor)
                {
                    goodList.Add(queueFloors[i]);
                }
                goodList.Sort((a, b) => a.CompareTo(b));
            }
            else if (direction == "down")
            {
                if (queueFloors[i] < currentFloor)
                {
                    goodList.Add(queueFloors[i]);
                }
                goodList.Sort((a, b) => b.CompareTo(a));
            }
        }

        if (goodList.Count == 0)
        {
            goodList.Add(0);
        }

        return goodList[0];
    }

    public void Add(int floor)
    {
        queueFloors.Add(floor);
        //queueFloors.Sort();
    }

    public void Remove(int floor)
    {
        queueFloors.Remove(floor);
    }
}
