using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddGameObjectToRuntimeSet : MonoBehaviour
{
    public GameObjectRuntimeSet GameObjectRuntimeSet;

    private void OnEnable()
    {
        GameObjectRuntimeSet.Assign(this.gameObject);
    }

    private void OnDisable()
    {
        //GameObjectRuntimeSet.A
    }
}