using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RuntimeSet<T> : ScriptableObject
{
    private T item;

    public T GetItem()
    {
        return item;
    }

    public void Assign(T assignItem)
    {
        item = assignItem;
    }

    /*
    public void Initialize()
    {
        item = null;
    }
    */

    /*
    public T GetItemIndex(int index)
    {
        return items[index];
    }

    public int GetItemsCount()
    {
        return items.Count;
    }

    public void AddToList(T objectToAdd)
    {
        if (!items.Contains(objectToAdd))
        {
            items.Add(objectToAdd);
        }
    }

    public void RemoveFromList(T objectToRemove)
    {
        if (items.Contains(objectToRemove))
        {
            items.Remove(objectToRemove);
        }
    }
    */
}
