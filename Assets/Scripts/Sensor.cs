using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : MonoBehaviour
{
    private Floor _floor;
    private int _floorNumber;

    private void Awake()
    {
        _floor = GetComponentInParent<Floor>();
        _floorNumber = _floor.FloorNumber;
    }

    private void Update()
    {
        /*
        Ray ray = new Ray(transform.position, transform.forward);
        Debug.DrawRay(transform.position, transform.forward * 100f, Color.yellow);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            hit.collider.gameObject.GetComponent<Elevator>().UpdateCurrentFloor(_floorNumber);
        }
        */
    }

    private void OnTriggerEnter(Collider collider)
    {
        collider.gameObject.GetComponent<Elevator>().UpdateCurrentFloor(_floorNumber);
        collider.gameObject.GetComponent<Elevator>().ToggleCanStop(true);
    }

    private void OnTriggerExit(Collider collider)
    {
        collider.gameObject.GetComponent<Elevator>().ToggleCanStop(false);
    }
}
